#!/bin/bash

systemctl enable firewalld --now
firewall-cmd --add-service=https --add-service=ssh --permanent
firewall-cmd --reload