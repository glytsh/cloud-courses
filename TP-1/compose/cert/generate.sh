#!/bin/sh

apk add --no-cache openssl

cd /cert

rm nginx.crt
rm nginx.key

openssl req -new -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3650 \
            -nodes \
            -out nginx.crt \
            -keyout nginx.key \
            -subj "/C=FR/ST=Gironde/L=Bordeaux/O=Ynov/OU=Informatique/CN=cloud-tp1"
