# TP 3 - Cloud Platform

## Introduction 

Le but de ce TP est de mettre en place un cloud privé grace a OpenNébula.

Opennebula est une solution complète permettant de gérer de façon centralisée plusieurs infrastructures virtuelles comme Vmware, Xen, KVM … OpenNebula opère comme un ordonnanceur des couches de stockage, réseau, supervision et de sécurité. OpenNebula se déploie aux cœurs des Datacenters. Elle s’adresse à n’importe quelle entreprise dotée d’une infrastructure informatique réseau complexe. OpenNebula peut être utilisé dans des datacenters privés par des organismes de recherche, des opérateurs de télécommunications et des intégrateurs de systèmes, ce qui donne à cette solution un avantage en terme de maturité d’usage.

## Technologies 

- OpenNébula
- OpenNébula-sunstone
- Vagrant 
- KVM