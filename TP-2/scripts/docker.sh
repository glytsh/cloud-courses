#/bin/env bash

systemctl enable --now docker
usermod -aG docker vagrant

mkdir /etc/docker
mv /tmp/daemon.json /etc/docker/