#!/bin/env bash

yum -y install epel-release
yum -y install s3fs-fuse

echo CECITESTLOLEXAMPLE:qszdZXUtnFDRZ/K7MDENG/BIBOUPFDEXAMPLEKEY > /etc/pwd-s3fs
echo CECITESTLOLEXAMPLE:qszdZXUtnFDRZ/GE9EDFG/BIBOUPFDEXAMPLEKEY > /home/vagrant/.pwd-s3fs
chown vagrant /home/vagrant/.pwd-s3fs
chmod 600 /home/vagrant/.pwd-s3fs

mkdir /data
chown vagrant:vagrant /data -R

sudo -u vagrant s3fs data /data -o no_check_certificate,use_path_request_style,url=https://minio.b3/,passwd_file=/home/vagrant/.passwd-s3fs &

mkdir -p /srv/backups
chown vagrant:vagrant /srv/backups -R

s3fs backups /srv/backups -o no_check_certificate,use_path_request_style,url=https://minio.b3/,passwd_file=/home/vagrant/.passwd-s3fs &
