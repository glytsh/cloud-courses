# TP2 : Orchestration de conteneurs et environnements flexibles

## I. Lab setup
| Noeud      | Rôle                 | Stockage                                |
|------------|----------------------|-----------------------------------------|
| `node1.b3` | Docker Swarm Manager | 1 disque système + 1 disque de données  |
| `node2.b3` | Docker Swarm Manager | 1 disque système + 1 disque de données  |
| `node3.b3` | Docker Swarm Manager | 1 disque système + 2 disques de données |


# II. Mise en place de Docker Swarm

## 1. Setup 

```
# Sur un premier noeud
docker swarm init --advertise-addr <LAN_IP>

# Obtenir un token pour ajouter des workers au cluster
docker swarm join-token worker

# Obtenir un token pour ajouter des managerss au cluster
docker swarm join-token manager

# Sur les autres noeuds
docker swarm join ... # réutiliser la commande obtenue avec le 'join-token'
```


## 2. Une première application orchestrée

```
$ docker stack deploy -c /path/to/docker-compose.yml stack_name
```

```
# Liste les stacks
docker stack ls

# Liste les services d'une stack
docker stack services <STACK_NAME>

# Récupère les logs d'un service
docker service logs <SERVICE_NAME>

# Récupère des infos sur un service
docker service ps <SERVICE_NAME>
```



# III. Construction de l'écosystème

## 1. Registre


```
# Récupération d'une image extérieure
docker pull alpine

# Renommage
docker tag <NAME> <REGISTRY>/<REPO_NAME>/<IMAGE_NAME>:<TAG>
docker tag alpine registry.b3:5000/alpine/alpine:test

# Push
docker push registry.b3:5000/alpine/alpine:test

# Récupération 
docker pull registry.b3:5000/alpine/alpine:test

# Et/ou utilisation
docker run -it registry.b3:5000/alpine/alpine:test sh
```


## 2. Centraliser l'accès aux services

```
# Ajout de la clause deploy, dans un service
services:
  example:
    networks:
      traefik:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.<SERVICE_NAME>.rule=Host(`<DOMAIN_NAME>`)"
        - "traefik.http.routers.<SERVICE_NAME>.entrypoints=web"
        - "traefik.http.services.<SERVICE_NAME>.loadbalancer.server.port=<SERVICE_PORT>"
        - "traefik.http.routers.<SERVICE_NAME>-tls.rule=Host(`<DOMAIN_NAME>`)"
        - "traefik.http.routers.<SERVICE_NAME>-tls.entrypoints=webtls"
        - "traefik.http.routers.<SERVICE_NAME>-tls.tls.certresolver=letsencryptresolver"
networks:
  traefik:
    external: true

# Exemple
services:
  python-webapp:
    networks:
      traefik:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.python-webapp.rule=Host(`python.webapp.tp3`)"
        - "traefik.http.routers.python-webapp.entrypoints=web"
        - "traefik.http.services.python-webapp.loadbalancer.server.port=8888"
        - "traefik.http.routers.python-webapp-tls.rule=Host(`python.webapp.tp3`)"  
        - "traefik.http.routers.python-webapp-tls.entrypoints=webtls"
        - "traefik.http.routers.python-webapp-tls.tls.certresolver=letsencryptresolver"
```


## 3. Swarm Management WebUI

## 4. Stockage S3

| Conteneur | Hôte       | Data Path  |
|-----------|------------|------------|
| `minio1`  | `node1.b3` | `/minio`   |
| `minio2`  | `node2.b3` | `/minio`   |
| `minio3`  | `node3.b3` | `/minio`   |
| `minio4`  | `node3.b3` | `/minio-2` |

```
docker node update --label-add minio1=true <DOCKER-NODE1>
docker node update --label-add minio2=true <DOCKER-NODE2>

# Puis deux fois le node3
docker node update --label-add minio3=true <DOCKER-NODE3>
docker node update --label-add minio4=true <DOCKER-NODE3>
```


## 5. Sauvegardes



## 6. Monitoring

### B. HomeMade : Prometheus

Ici a été déployé un Prometheus couplé d'un grafana afin de monitorer l'avancement des conteneurs