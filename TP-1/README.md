# TP1 Prise en main de Docker

Ce TP a pour but de prendre en main l'outil `docker`, sans rentrer dans les détails de son implémentation ou de son écosystème.  

L'objectif est de vous rendre autonome sur le lancement et la création de services conteneurisés. 

<!-- vim-markdown-toc GitLab -->

* [0. Setup](#0-setup)
* [I. Prise en main](#i-prise-en-main)
    * [2. Gestion d'images](#2-gestion-dimages)
        * [Gestion d'images](#gestion-dimages)
        * [Création d'image](#création-dimage)
    * [3. Manipulation du démon docker](#3-manipulation-du-démon-docker)
* [II. `docker-compose`](#ii-docker-compose)
    * [Basics](#basics)
    * [Write your own](#write-your-own)
    * [Rangement](#rangement)
    * [Conteneuriser une application donnée](#conteneuriser-une-application-donnée)

<!-- vim-markdown-toc -->

# 0. Setup

Machine CentOS :
* réseau configuré
* SSH fonctionnel
* utilisation d'un user qui a accès aux droits de `root` *via* `sudo`
* pour que le partage de ports Docker fonctionne avec `firewalld` activé
  * `sudo firewall-cmd --add-masquerade --zone=public --permanent` puis un `sudo firewall-cmd --reload`

Installation de Docker :
* en suivant [la doc officielle](https://docs.docker.com/engine/install/centos/)
* lancer le démon, maintenant (`start`) et au démarrage (`enable`)
* donner accès à l'utilisateur courant au démon Docker
* vérifier le bon fonctionnement de l'installation de Docker

# I. Prise en main

## 1. Lancer des conteneurs

Ici on va manipuler certaines des commandes Docker élémentaires :
* `docker run`
* `docker ps` ou `docker container ls`
* `docker exec`
* `docker rm`

---

## 2. Gestion d'images

Voir [l'intro du cours.](../cours/containers_and_docker.md#images-docker)

### Gestion d'images

* récupération d'une image : `docker pull`
  * dans son utilisation la plus simple, `docker pull` utilise le Docker Hub par défaut (on reviendra sur ça quand vous monterez votre propre *registry*).
  * `docker pull <IMAGE>:<TAG>` pour les images du dépôt *library*
  * `docker pull <REPO>/<IMAGE>:<TAG>` pour les autres images
  * 🌞 récupérer une image de Apache en version 2.2
    * la lancer en partageant un port qui permet d'accéder à la page d'accueil d'Apache

    🤟 Commande utilisée : ``` docker run -f --name apacheserver -p 80:80 httpd```


* on peut lister et gérer les images avec `docker image`
  * `docker image ls` : liste les images
  * `docker image rm <IMAGE>:<TAG>` : supprime une image

> Si on `docker run` une image qui n'a pas été récupérée avant, elle est `docker pull automatiquement`

### Création d'image

Pour créer une image : 
* création d'un fichier Dockerfile
* `cd /path/to/dockerfile/directory`
* exécution de `docker build -t <IMAGE_NAME> .`
  * le `.` en fin de ligne est important : il indique que le Dockerfile est dans le répertoire courant
* lancer le conteneur avec `docker run <IMAGE_NAME>`

---

* 🌞 créer une image qui lance un serveur web python. L'image doit :
  * se baser sur alpine (clause `FROM`)
  * contenir `python3` (clause `RUN`)
  * utiliser la clause `EXPOSE` afin d'être plus explicite
    * **NB : la clause EXPOSE ne partage aucun port**, c'est simplement utilisé pour préciser de façon explicite que ce conteneur écoutera sur un port donné
    * cela permet à quelqu'un qui analyse votre image de savoir rapidement quel port sera utilisé par un conteneur donné
  * contenir une clause `WORKDIR` afin de spécifier un répertoire de travail (les commandes suivantes seront lancées depuis ce répertoire)
  * utiliser `COPY` pour récupérer un fichier à partager à l'aide du serveur HTTP
  * lancer la commande `python3 -m http.server 8888` (clause `CMD`)
    * cela lance un serveur web qui écoute sur le port TCP 8888 de toutes les interfaces du conteneur

  🤟 Voir Dockerfile

---

> **Vous devez réutilisez l'image créée dans le point précédent**

* 🌞 lancer le conteneur et accéder au serveur web du conteneur depuis votre PC
  * avec un `docker run` et les bonnes options
  * il faudra faire un partage de port (`-p`) pour pouvoir partager le port du conteneur vers un port de l'hôte
  * par exemple `docker run -p 7777:8080 <IMAGE>` permet de partager le port 7777 de l'hôte vers le port 8080 du conteneur (TCP par défaut)
  
  🤟 
  ![serverpython](./images/serverpython.png)

  puis:

  Docker stop serverpython
  Docker rm serverpython

  🤟 Ajouter un volume :

  ``` docker run -d -p 8080:8888 -v $(pwd):/workspace --name serverpython serverpython ```



```
PC ---> VM IP:port ---> Conteneur IP:port
```

* 🌞 utiliser l'option `-v` de `docker run` 
  * c'est un **volume Docker**
  * on utilise les volumes pour partager des fichiers de l'hôte dans le conteneur
  * vous devrez monter le répertoire de votre choix, dans le `WORKDIR` du conteneur
  * la syntaxe est la suivante :

```
docker run -v <PATH_ON_HOST>:<PATH_IN_CT> <IMAGE_NAME>
```

## 3. Manipulation du démon docker

Emplacement du socket : 

🤟 ``` /var/run/docker.socket ```

# II. `docker-compose`

## Basics

Créer un répertoire de travail qui contient le fichier `docker-compose.yml` avec le contenu qui suit :

```
version: "3.3"

services:
  server:
    image: nginx
    networks:
      nginx-net:
        aliases:
          - nginx.test
          - nginx
    ports:
      - "8080:80"

networks:
  nginx-net:
```

Il est possible d'interagir avec en utilisant les commandes : 
```
# Lancer les conteneurs
$ docker-compose up

# Couper les conteneurs
$ docker-compose down

# Demander le build de tous les conteneurs si un Dockerfile est renseigné 
$ docker-compose build

# Obtenir des infos sur les conteneurs qui tournent
$ docker-compose ps
$ docker-compose top
$ docker stats
```

## Write your own

🌞 Ecrire un `docker-compose-v1.yml` qui permet de :
* lancer votre image de serveur web Python créée en [2.](#cr%c3%a9ation-dimage)
* partage le port TCP du conteneur sur l'hôte
* faire en sorte que le conteneur soit build automatiquement si ce n'est pas fait

> On peut voir les logs d'un conteneurs qui tourne en démon avec `docker logs <ID_OR_NAME>`.

---

🌞 Ajouter un deuxième conteneur `docker-compose-v2.yml`
* ajouter un conteneur NGINX dans le `docker-compose-v2.yml`
  * réutiliser un conteneur NGINX existant (pas de nouveau Dockerfile)
* celui-ci doit agir comme reverse proxy vers votre serveur Python
  * il va falloir produire une configuration NGINX
    * la configuration doit être monté avec un volume au lancement du conteneur
    * si vous êtes pas à l'aise avec NGINX et sa config, cf le petit encart en dessous de cette liste
  * la connexion au conteneur NGINX doit se faire en HTTPS
    * le certificat et la clé pour la connexion doivent être générés avant le lancement du conteneur
    * ils sont montés avec un volume au lancement du conteneur
  * le port du conteneur NGINX doit être exposé sur l'hôte sur le port 443
  * le port du serveur web n'est plus exposé sur l'hôte
* [utiliser les `aliases` network](https://docs.docker.com/compose/compose-file/#aliases) pour que vos conteneurs communiquent entre eux

```
Client Web ---> VM ---> CT NGINX ---> CT Python
```

> Pour la configuration NGINX, si vous n'êtes pas trop familier, vous pourrez trouver un [exemple de configuration fonctionnelle dans le dépôt](./nginx/test.docker.conf). Cette configuration :  
> * suppose que vous avez déjà généré une paire de clé/certificats (avec pour nom `test.docker`)
> * suppose que votre clés et certificats sont dans `/certs`
> * suppose que votre app Python est joignable à l'adresse `python-app` sur le port `8888/TCP`
> * suppose que vous souhaitez joindre le service sur `https://test.docker`
> * le ficher de conf fourni est à déposer dans `/etc/nginx/conf.d/` (dans le conteneur) sous le nom `test.docker.conf` 

## Rangement

A l'issue de cette étape, vous devez fournir une arborescence de fichiers fonctionnels avec :
* un Dockerfile (serveur web Python) 
* les `docker-compose.yml`
  * `docker-compose-v1.yml`
  * `docker-compose-v2.yml`
* une configuration NGINX
* des certificats pour NGINX

Je vous conseille l'arborescence suivante :
```
├── README.md
├── docker-compose-v1.yml
├── docker-compose-v2.yml
├── nginx
│   ├── certs
│   │   ├── nginx.crt
│   │   └── nginx.key
│   └── conf
│       └── nginx.conf
└── webserver
    └── Dockerfile
```

🌞 Le fichier `README.md` doit contenir des instructions simples sur le lancement de l'application avec `docker-compose`, et une explication succinte des différents composants

## Conteneuriser une application donnée

Ici on se rapproche d'un cas d'utilisation réel : je vous mets une application sur les bras et vous devez la conteneuriser. 

L'application : 
* codée en `python3`
  * [les sources sont dans ici](./python-app)
    * pour rappel, elle écoute sur le port 8888/tcp
  * n'hésitez pas à cloner mon repo pour copier directement les fichiers
* nécessite des librairies installables avec `pip`
  * `pip install -r <FICHIER>`
* a besoin d'un Redis pour fonctionner
  * il doit être joignable sur le nom `db` (port par défaut (6379/TCP))

🌞 Vous devez :
* construire une image qui
  * contient `python3`
  * contient l'application et ses dépendances
  * lance l'application au démarrage du conteneur
* écrire un `docker-compose.yml`
  * contient l'application
  * contient un Redis
    * utilise l'image de *library*
    * a un alias `db`
  * contient un NGINX
    * reverse proxy HTTPS vers l'application Web
    * a son port 443 exposé

Structure attendue :
```
├── README.md
├── docker-compose.yml
├── nginx
│   ├── certs
│   │   ├── test.docker.crt
│   │   └── test.docker.key
│   └── conf.d
│       └── test.docker.conf
└── webserver
    ├── app
    │   ├── app.py
    │   ├── requirements
    │   └── templates
    └── Dockerfile
```