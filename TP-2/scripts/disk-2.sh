#!/bin/bash

yum -y install lvm2

systemctl enable --now lvm2-lvmetad
systemctl enable --now lvm2-monitor

pvcreate -v /dev/sdc
vgcreate -s 256k -v vg_minio /dev/sdc
lvcreate -l +100%FREE -v vg_minio -n lv_minio

mkfs.ext4 /dev/vg_minio/lv_minio

mkdir /minio

mount /dev/vg_minio/lv_minio /minio