#/bin/env bash

echo "10.33.0.21 registry.b3" >> /etc/hosts
echo "10.33.0.21 traefik.b3" >> /etc/hosts
echo "10.33.0.21 python.b3" >> /etc/hosts

cat > /etc/hosts <<EOF> 
10.33.0.21 registry.b3
EOF

cat > /etc/hosts <<EOF> 
10.33.0.21 traefik.b3
EOF

cat > /etc/hosts <<EOF> 
10.33.0.21 python.b3
EOF